import { groupBy } from 'lodash'
import { Product } from './thinking-in-react/Product'

export const capitalize = (word: string) => {
  return word[0].toUpperCase() + word.slice(1, word.length)
}

export const camelCaseToPhrase = (word: string) => {
  let str = ''
  word.split('').forEach(s => {
    if (s === s.toUpperCase()) {
      str = `${str} ${s.toLowerCase()}`
    } else {
      str = str + s
    }
  })
  return capitalize(str)
}

export const roll = (max: number, min: number): number => {
  return Math.floor(Math.random() * (max - min) + min)
}
function groupByKey<T, K extends keyof T & string>(
  arr: ReadonlyArray<T>,
  prop: K,
): Record<any, T[]> {
  const result: any = {}
  for (const product of arr) {
    const key = product[prop]
    if (result[key]) {
      result[key].push(product)
    } else {
      result[key] = [product]
    }
  }
  return result
}

export const filterByCategory = (products: ReadonlyArray<Product>) =>
  Object.entries(groupByKey(products, 'category'))
