import React from 'react'

interface NasaPhoto {
  readonly title: string
  readonly url: string
  readonly explanation: string
  readonly date: string
}

interface NasaPhotoProps {
  readonly nasaPhoto: NasaPhoto
}

export const NasaPhotoView: React.FC<NasaPhotoProps> = ({ nasaPhoto }) => {
  return (
    <>
      <img src={nasaPhoto.url} />
      <h1>{nasaPhoto.title}</h1>
      <p>{nasaPhoto.date}</p>
      <p>{nasaPhoto.explanation}</p>
    </>
  )
}

export const NasaPhotoApp: React.FC = () => {
  const [nasaPhoto, setNasaPhoto] = React.useState<any>({})

  const apiKey = 'cZFrFJNKU05rn5rMWmUrKOl6qp13Fb5vaxdR4hD2'

  React.useEffect(() => {
    fetch(`https://api.nasa.gov/planetary/apod?api_key=${apiKey}`)
      .then(res => res.json())
      .then(data =>
        setNasaPhoto({
          url: data.url,
          title: data.title,
          date: data.date,
          explanation: data.explanation,
        }),
      )
      .catch(err => console.log(err))
  }, [])
  return <NasaPhotoView nasaPhoto={nasaPhoto} />
}
