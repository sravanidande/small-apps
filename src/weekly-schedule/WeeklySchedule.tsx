import React from 'react'
import { WeeklySchedule } from './model'

interface WeeklyScheduleViewProps {
  data: WeeklySchedule
}

export const WeeklyScheduleView: React.FC<WeeklyScheduleViewProps> = ({
  data,
}) => {
  const dayCompletedTasksCount = data.map(dayData =>
    dayData.tasks.reduce((acc, v) => {
      const count = 0
      return v.completed ? acc + (count + 1) : acc
    }, 0),
  )
  console.log(dayCompletedTasksCount)

  const weeklyCompletedTasksCount = dayCompletedTasksCount.reduce(
    (acc, v) => acc + v,
    0,
  )

  console.log(weeklyCompletedTasksCount)
  return (
    <>
      <div className="header">Weekly Schedule ✅</div>
      <div className="countBar">
        Weekly completed tasks={weeklyCompletedTasksCount}
      </div>
      <div className="main">
        {data.map((dayData, i) => (
          <div key={dayData.id} className="day">
            <div style={{ fontWeight: 'bold' }}>
              {dayData.day} - {dayCompletedTasksCount[i]} completed
            </div>
            {dayData.tasks.map(task => {
              const styles = { textDecoration: 'line-through' }
              const styledCompleted = task.completed ? styles : {}
              return (
                <div key={task.id} className="tasks">
                  <input
                    type="checkbox"
                    name="task1"
                    id="task1"
                    checked={task.completed}
                    readOnly
                  />
                  <label htmlFor="task1" style={styledCompleted}>
                    {task.title}
                  </label>
                </div>
              )
            })}
          </div>
        ))}
      </div>
      <div className="footer">copy right by @coder community!</div>
    </>
  )
}
