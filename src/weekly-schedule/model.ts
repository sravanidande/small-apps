import {
  enums,
  TypeOf,
  req,
  number,
  string,
  boolean,
  readonlyArray,
  IntID,
} from 'technoidentity-utils'

export const DayOfWeek = enums(
  'DayOfWeek',
  'Sun',
  'Mon',
  'Tue',
  'Wed',
  'Thu',
  'Fri',
  'Sat',
)

export type DayOfWeek = TypeOf<typeof DayOfWeek>

export const Task = req({
  id: number,
  title: string,
  completed: boolean,
})

export type Task = TypeOf<typeof Task>

export const Tasks = readonlyArray(Task)

export type Tasks = TypeOf<typeof Tasks>

export const DaySchedule = req({
  id: number,
  day: DayOfWeek,
  tasks: Tasks,
})

export type DaySchedule = TypeOf<typeof DaySchedule>

export const WeeklySchedule = readonlyArray(DaySchedule)

export type WeeklySchedule = TypeOf<typeof WeeklySchedule>
