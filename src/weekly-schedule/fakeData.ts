import { Task, Tasks, DayOfWeek } from './model'
import { checked, date, number } from 'technoidentity-utils'
import { addDays } from 'date-fns'
import { range } from '../makeData'
import Chance from 'chance'

const ch = new Chance()

export const createTask = () => {
  return {
    id: ch.integer({ min: 1, max: 10000 }),
    title: ch.word({ length: 5 }),
    completed: ch.bool(),
  }
}

export const followingDay = checked([date], date, currentDay => {
  return addDays(currentDay, 1)
})

export const createTasks = checked([number], Tasks, n => {
  return range(0, n).map(_ => createTask())
})

export const createWeeklySchedule = () => {
  const days = DayOfWeek.keys
  return days.map(day => {
    return {
      id: ch.integer({ min: 1, max: 10000 }),
      day: day,
      tasks: createTasks(ch.integer({ min: 2, max: 5 })),
    }
  })
}

export const weeklyScheduledData = createWeeklySchedule()
