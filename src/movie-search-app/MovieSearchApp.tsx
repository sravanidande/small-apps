import React from 'react'

export const Title: React.FC = () => {
  return (
    <>
      <h1 className="title">React Movie Search</h1>
    </>
  )
}

interface SearchMovieViewProps {
  onSearchMovie(query: string): void
}

export const SearchMovieView: React.FC<SearchMovieViewProps> = ({
  onSearchMovie,
}) => {
  const [movie, setMovie] = React.useState('')
  return (
    <form className="form">
      <label htmlFor="query" className="label">
        Movie Name
      </label>
      <input
        className="input"
        type="text"
        name="query"
        value={movie}
        onChange={evt => setMovie(evt.target.value)}
        placeholder="Search for movie"
      />
      <button
        type="submit"
        className="button"
        onClick={evt => {
          evt.preventDefault()
          onSearchMovie(movie)
        }}
      >
        Search
      </button>
    </form>
  )
}

interface MovieDisplayProps {
  readonly title: string
}

export const Movie: React.FC<MovieDisplayProps> = ({ title }) => {
  return (
    <li>
      <p>{title}</p>
    </li>
  )
}

interface MovieListProps {
  readonly movieList: ReadonlyArray<string>
}

export const MovieList: React.FC<MovieListProps> = ({ movieList }) => {
  return (
    <ul>
      {movieList.map((movie, index) => (
        <Movie key={index} title={movie} />
      ))}
    </ul>
  )
}

export const MovieSearchApp: React.FC = () => {
  const [query, setQuery] = React.useState('')
  const [movieList, setMovieList] = React.useState([])

  React.useEffect(() => {
    if (query.length !== 0) {
      fetch(
        `https://api.themoviedb.org/3/search/movie?api_key=e5a517fa2a2b668737d9cd911477868e&language=en-US&query=${query}&page=1&include_adult=false`,
      )
        .then(res => res.json())
        .then(data => data.results)
        .then(data => data.map((movie: any) => movie.title))
        .then(setMovieList)
        .catch(err => console.log(err.name))
    }
  }, [query])

  const handleQuerySearch = (query: string) => {
    setQuery(query.trim())
  }

  return (
    <>
      <Title />
      <SearchMovieView onSearchMovie={handleQuerySearch} />
      <MovieList movieList={movieList} />
    </>
  )
}
