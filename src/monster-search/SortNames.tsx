import React from 'react'
import { Button } from '@material-ui/core'

interface SortNamesProps {
  readonly sort: 'asc' | 'desc'
  onSortChange(sort: 'asc' | 'desc'): void
}

export const SortNames: React.FC<SortNamesProps> = ({ sort, onSortChange }) => {
  return (
    <Button
      color="secondary"
      variant="contained"
      onClick={() => onSortChange(sort)}
    >
      {sort === 'asc' ? 'sorted A-Z' : 'sorted Z-A'}
    </Button>
  )
}
