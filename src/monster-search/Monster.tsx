import React from 'react'
import './monster.styles.css'

export interface Monster {
  readonly id: number
  readonly name: string
  readonly email: string
}

interface MonsterViewProps {
  readonly monster: Monster
}

export const MonsterView: React.FC<MonsterViewProps> = ({ monster }) => {
  return (
    <div className="card-container">
      <img src={`https://robohash.org/${monster.id}?set=set2`} />
      <h2>{monster.name}</h2>
      <p>{monster.email}</p>
    </div>
  )
}
