import React from 'react'
import { MonsterSearchInput } from './MonsterSearchInput'
import { MonsterList } from './MonsterList'
import './monsterApp.styles.css'
import { Monster } from './Monster'
import {
  AppBar,
  Toolbar,
  IconButton,
  Typography,
  Button,
} from '@material-ui/core'
import { SortNames } from './SortNames'

export const MonsterSearchApp: React.FC = () => {
  const [search, set] = React.useState('')
  const [monsters, setList] = React.useState([])
  const [sort, setSort] = React.useState<'asc' | 'desc'>('asc')

  React.useEffect(() => {
    fetch('https://jsonplaceholder.typicode.com/users')
      .then(res => res.json())
      .then(setList)
      .catch(err => console.log(err.message))
  }, [search])

  const handleSearchMonster = (monster: string) => {
    set(monster)
  }

  console.log(monsters)

  console.log(sort, 'initial')

  const sortByNames = (a: any, b: any): any => {
    const nameA = a.name.toUpperCase()
    const nameB = b.name.toUpperCase()
    if (nameA < nameB) {
      return -1
    }
    if (nameA > nameB) {
      return 1
    }
    return 0
  }

  const sortedMonsters =
    sort === 'asc'
      ? monsters.sort(sortByNames)
      : monsters.sort(sortByNames).reverse()

  const sortedFilteredMonsters = sortedMonsters.filter((monster: Monster) =>
    monster.name.toLowerCase().includes(search.toLowerCase()),
  )

  const handleSortChange = (sort: 'asc' | 'desc') => {
    sort === 'asc' ? setSort('desc') : setSort('asc')
  }

  return (
    <>
      <MonsterSearchInput
        monster={search}
        onSearchMonster={handleSearchMonster}
      />
      <SortNames sort={sort} onSortChange={handleSortChange} />

      <MonsterList monsterList={sortedFilteredMonsters} />
    </>
  )
}
