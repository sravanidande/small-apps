import React from 'react'
import { Monster } from './Monster'

interface MonsterSearchInputProps {
  readonly monster: string
  onSearchMonster(monster: string): void
}

export const MonsterSearchInput: React.FC<MonsterSearchInputProps> = ({
  monster,
  onSearchMonster,
}) => {
  return (
    <input
      className="input"
      type="text"
      name="monster"
      value={monster}
      onChange={evt => onSearchMonster(evt.target.value)}
      placeholder="Search monster here..."
    />
  )
}
