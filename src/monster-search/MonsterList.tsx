import React from 'react'
import { Monster, MonsterView } from './Monster'
import './monsterList.styles.css'

interface MonsterListProps {
  readonly monsterList: ReadonlyArray<Monster>
}

export const MonsterList: React.FC<MonsterListProps> = ({ monsterList }) => {
  return (
    <div className="card-list">
      {monsterList.length !== 0 ? (
        monsterList.map(monster => (
          <MonsterView key={monster.id} monster={monster} />
        ))
      ) : (
        <h3>oops! result not found</h3>
      )}
    </div>
  )
}
