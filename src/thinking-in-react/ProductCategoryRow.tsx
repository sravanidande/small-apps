import React from 'react'
import { Product } from './Product'
import { Th, Tr } from 'technoidentity-ui'

interface ProductCategoryRowProps {
  readonly category: Product['category']
}

export const ProductCategoryRow: React.FC<ProductCategoryRowProps> = ({
  category,
}) => {
  return (
    <Tr>
      <Th>{category}</Th>
    </Tr>
  )
}
