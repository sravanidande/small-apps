import React from 'react'
import { filterByCategory } from '../utils'

export interface Product {
  readonly id: number
  readonly category: string
  readonly price: string
  readonly stocked: boolean
  readonly name: string
}

export const PRODUCTS: ReadonlyArray<Product> = [
  {
    id: 1,
    category: 'Sporting Goods',
    price: '$49.99',
    stocked: true,
    name: 'Football',
  },
  {
    id: 2,
    category: 'Sporting Goods',
    price: '$9.99',
    stocked: true,
    name: 'Baseball',
  },
  {
    id: 3,
    category: 'Sporting Goods',
    price: '$29.99',
    stocked: false,
    name: 'Basketball',
  },

  {
    id: 4,
    category: 'Electronics',
    price: '$899.99',
    stocked: true,
    name: 'iPad',
  },
  {
    id: 5,
    category: 'Electronics',
    price: '$399.99',
    stocked: false,
    name: 'iPhone 11',
  },
  {
    id: 6,
    category: 'Electronics',
    price: '$199.99',
    stocked: true,
    name: 'Galaxy S20',
  },
]

export const productsByCategory = filterByCategory(PRODUCTS)

export type ProductsByCategory = typeof productsByCategory
