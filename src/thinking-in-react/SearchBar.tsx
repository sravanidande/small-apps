import React from 'react'
import { Input, CheckBox, Title } from 'technoidentity-ui'
import { productsByCategory, ProductsByCategory, Product } from './Product'
import { ProductListView } from './ProductListView'

export const filteredProducts = (
  text: string,
  arr: ProductsByCategory,
): ProductsByCategory => {
  const result: ProductsByCategory = []
  for (const prod of arr) {
    const [category, ...productsListByCategory] = prod
    for (const productList of productsListByCategory) {
      const productsResult: Product[] = []
      for (const product of productList) {
        if (product.name.toLowerCase().includes(text.toLowerCase())) {
          productsResult.push(product)
        }
      }
      result.push([category, productsResult])
    }
  }
  return result
}

export const checkedProducts = (
  arr: ProductsByCategory,
): ProductsByCategory => {
  const result: ProductsByCategory = []
  for (const prod of arr) {
    const [category, ...productsListByCategory] = prod
    for (const productList of productsListByCategory) {
      const productsResult: Product[] = []
      for (const product of productList) {
        if (product.stocked) {
          productsResult.push(product)
        }
      }
      result.push([category, productsResult])
    }
  }
  return result
}

export const FilteredSearchForm = () => {
  const [searchText, setSearchText] = React.useState('')
  const [checked, setChecked] = React.useState(false)

  const handleSearchText = (item: string) => {
    setSearchText(item)
  }

  const handleCheck = () => {
    setChecked(!checked)
  }

  const finalProducts =
    searchText.length === 0
      ? productsByCategory
      : filteredProducts(searchText, productsByCategory)

  const finalCheckedProducts = checked
    ? checkedProducts(finalProducts)
    : finalProducts

  return (
    <>
      <Title size="3" textAlignment="centered">
        Products
      </Title>
      <Input
        type="search"
        value={searchText}
        onChange={evt => handleSearchText(evt.target.value)}
        placeholder="Search..."
      />
      <CheckBox name="checked" checked={checked} onChange={handleCheck}>
        Only show products in stock
      </CheckBox>
      <ProductListView productList={finalCheckedProducts} />
    </>
  )
}
