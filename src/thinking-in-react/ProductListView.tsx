import { TableBody, TableHead, Container } from '@material-ui/core'
import React from 'react'
import { Table, Th, Tr, Section } from 'technoidentity-ui'
import { Product, ProductsByCategory } from './Product'
import { ProductCategoryRow } from './ProductCategoryRow'
import { ProductRow } from './ProductRow'

interface ProductListViewProps {
  readonly productList: ProductsByCategory
}

export const ProductListView: React.FC<ProductListViewProps> = ({
  productList,
}) => {
  return (
    <Container>
      <Table striped>
        <TableHead>
          <Tr>
            <Th>Name</Th>
            <Th>price</Th>
          </Tr>
        </TableHead>
        <TableBody>
          {productList.map((product, index) => (
            <React.Fragment key={product[0]}>
              <ProductCategoryRow category={product[0]} />
              {product[1].map((item: Product) => (
                <ProductRow
                  key={item.name}
                  stocked={item.stocked}
                  name={item.name}
                  price={item.price}
                />
              ))}
            </React.Fragment>
          ))}
        </TableBody>
      </Table>
    </Container>
  )
}
