import React from 'react'
import { Tr, Td, Th } from 'technoidentity-ui'
import { Product } from './Product'

interface ProductRowProps {
  readonly name: Product['name']
  readonly price: Product['price']
  readonly stocked: Product['stocked']
}

export const ProductRow: React.FC<ProductRowProps> = ({
  name,
  price,
  stocked,
}) => {
  return (
    <Tr>
      {!stocked ? <Td style={{ color: 'red' }}>{name}</Td> : <Td>{name}</Td>}
      <Td>{price}</Td>
    </Tr>
  )
}
