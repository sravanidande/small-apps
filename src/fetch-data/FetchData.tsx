import React from 'react'

interface Todo {
  readonly id: number
  readonly title: string
  readonly completed: boolean
}

interface TodoProps {
  readonly fetchedTodo: Todo
}

export const TodoView: React.FC<TodoProps> = ({ fetchedTodo }) => {
  return (
    <>
      <p>Id:{fetchedTodo.id}</p>
      <p>Title:{fetchedTodo.title}</p>
      <p>Completed:{fetchedTodo.completed}</p>
    </>
  )
}

export const TodoFetch: React.FC = () => {
  const [fetchedTodo, setFetchedTodo] = React.useState<Todo | undefined>()
  const [error, setError] = React.useState(null)

  React.useEffect(() => {
    async function fetchTodo() {
      try {
        const fetchedOne = await window.fetch(
          'https://jsonplaceholder.typicode.com/todos/1',
        )
        const res = await fetchedOne.json()
        setFetchedTodo({
          id: res.id,
          title: res.title,
          completed: res.completed,
        })
      } catch (error) {
        setError(error)
      }
    }
    fetchTodo()
  }, [])

  return fetchedTodo ? (
    <TodoView fetchedTodo={fetchedTodo} />
  ) : error ? (
    <h1>Error!</h1>
  ) : (
    <h1>Loading</h1>
  )
}
