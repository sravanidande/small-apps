import firebase from 'firebase/app'
import 'firebase/firestore'
import 'firebase/auth'

const config = {
  apiKey: 'AIzaSyD1sg7RfAUHIb8qyfgKy3_Z36skCsAEePs',
  authDomain: 'e-commerce-db-b2f02.firebaseapp.com',
  databaseURL: 'https://e-commerce-db-b2f02.firebaseio.com',
  projectId: 'e-commerce-db-b2f02',
  storageBucket: 'e-commerce-db-b2f02.appspot.com',
  messagingSenderId: '686809269975',
  appId: '1:686809269975:web:6f144e7db245c37e399292',
  measurementId: 'G-RKPNNYTXR5',
}

firebase.initializeApp(config)

export const auth = firebase.auth()
export const firestore = firebase.firestore()

const provider = new firebase.auth.GoogleAuthProvider()

provider.setCustomParameters({ prompt: 'select_account' })

export const signInWithGoogle = () =>
  auth
    .signInWithPopup(provider)
    .then(() => console.log('helo'))
    .catch(err => console.log(err.message))

export default firebase
