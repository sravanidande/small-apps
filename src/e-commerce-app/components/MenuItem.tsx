import * as React from 'react'
import { useRouteMatch, useHistory } from 'react-router-dom'

export interface MenuItemProps {
  readonly id?: number
  readonly title: string
  readonly imageUrl: string
}

export const MenuItem: React.FC<MenuItemProps> = ({ title, imageUrl }) => {
  const match = useRouteMatch()
  const history = useHistory()
  return (
    <div
      style={{ backgroundImage: `url(${imageUrl})` }}
      className="menu-item"
      onClick={() => history.push(`${match.url}${title}`)}
    >
      <div className="content">
        <h1 className="title">{title.toUpperCase()}</h1>
        <span className="subtitle">SHOP NOW </span>
      </div>
    </div>
  )
}
