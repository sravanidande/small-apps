import React from 'react'
import { CollectionItem } from '../pages/shopData'
import './collectionItem.styles.scss'

interface CollectionItemViewProps {
  readonly collectionItem: CollectionItem
}

export const CollectionItemView: React.FC<CollectionItemViewProps> = ({
  collectionItem,
}) => {
  return (
    <div className="collection-item">
      <img
        className="image"
        style={{ backgroundImage: `url(${collectionItem.imageUrl})` }}
      />
      <div className="collection-footer">
        <div className="name">{collectionItem.name}</div>
        <div className="price">{collectionItem.price}</div>
      </div>
    </div>
  )
}
