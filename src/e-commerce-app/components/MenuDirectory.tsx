import React from 'react'
import { MenuItemProps, MenuItem } from './MenuItem'
import './menu.styles.scss'

interface MenuDirectoryProps {
  readonly menuItems: ReadonlyArray<MenuItemProps>
}

export const MenuDirectory: React.FC<MenuDirectoryProps> = ({ menuItems }) => {
  return (
    <div className="directory-menu">
      {menuItems.map(item => (
        <MenuItem key={item.id} title={item.title} imageUrl={item.imageUrl} />
      ))}
    </div>
  )
}
