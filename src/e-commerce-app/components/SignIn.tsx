import React from 'react'
import './signIn.styles.scss'
import './button.styles.scss'
import { signInWithGoogle } from '../firebase/firebase'

export interface User {
  readonly email: string
  readonly password: string
}

interface SignInProps {
  onSignIn(values: User): void
  readonly isGoogleSignIn: boolean
}

export const SignIn: React.FC<SignInProps> = ({ isGoogleSignIn, onSignIn }) => {
  const [email, setEmail] = React.useState('')
  const [password, setPassword] = React.useState('')
  return (
    <div className="sign-in">
      <h2>I already have an account</h2>
      <span>Sign in with email and password</span>

      <form>
        <div className="group">
          <label className="shrink">Email</label>
          <input
            className="form-input"
            type="text"
            name="email"
            value={email}
            onChange={evt => setEmail(evt.target.value)}
            required
          />
        </div>
        <div className="group">
          <label className="shrink">Password</label>
          <input
            className="form-input"
            type="password"
            name="password"
            value={password}
            onChange={evt => setPassword(evt.target.value)}
            required
          />
        </div>
        <div className="buttons">
          <button
            type="submit"
            className="custom-button"
            onClick={evt => {
              evt.preventDefault()
              onSignIn({ email, password })
            }}
          >
            SignIn
          </button>
          <button
            type="button"
            className={`${
              isGoogleSignIn ? 'google-sign-in' : ''
            } custom-button`}
            onClick={signInWithGoogle}
          >
            SignIn With Google
          </button>
        </div>
      </form>
    </div>
  )
}
