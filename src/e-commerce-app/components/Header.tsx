import Logo from './crown.svg'
import React from 'react'
import { Link } from 'react-router-dom'
import './header.styles.scss'
import { User } from './SignIn'
import { auth } from '../firebase/firebase'

interface HeaderProps {
  readonly currentUser: User
  onUserChange(user: User): void
}

export const Header: React.FC<HeaderProps> = ({
  currentUser,
  onUserChange,
}) => {
  return (
    <div className="header">
      <Link className="logo-container" to="/">
        <Logo className="logo" />
      </Link>
      <div className="options">
        <Link className="option" to="/shop">
          SHOP
        </Link>
        <Link className="option" to="/">
          CONTACT
        </Link>
        {currentUser ? (
          <div className="option" onClick={() => auth.signOut()}>
            SIGN OUT
          </div>
        ) : (
          <Link
            className="option"
            onClick={() => onUserChange(currentUser)}
            to="/signin"
          >
            SIGN IN
          </Link>
        )}
      </div>
    </div>
  )
}
