import React from 'react'
import { CollectionItemView } from '../components/CollectionItem'
import { Collection } from './shopData'
import './shopPage.styles.scss'
import '../../styles2.css'

interface ShopPageViewProps {
  readonly collections: ReadonlyArray<Collection>
}

export const ShopPageView: React.FC<ShopPageViewProps> = ({ collections }) => {
  return (
    <div className="collection-preview">
      {collections.map(collection => (
        <div key={collection.id}>
          <h1 className="title">{collection.title.toUpperCase()}</h1>
          <div className="preview">
            {collection.items
              .filter((item, idx) => idx < 4)
              .map(item => (
                <CollectionItemView key={item.id} collectionItem={item} />
              ))}
          </div>
        </div>
      ))}
    </div>
  )
}
