import React from 'react'
import { signInWithGoogle } from '../firebase/firebase'
import { SignIn } from '../components/SignIn'

export const SignInPageView: React.FC = () => {
  return (
    <SignIn isGoogleSignIn={true} onSignIn={values => console.log(values)} />
  )
}
