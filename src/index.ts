import React from 'react'
import { render } from 'react-dom'
import { App } from './App'
import './todoData'

render(React.createElement(App), document.getElementById('root'))
