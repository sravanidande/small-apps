import { number, boolean, string } from 'io-ts'

export interface Todo {
  readonly taskId: number
  readonly title: string
  readonly taskDone: boolean
}

export const todos: ReadonlyArray<Todo> = [
  { taskId: 2, title: 'work', taskDone: false },
  { taskId: 1, title: 'sleep', taskDone: true },
  { taskId: 4, title: 'eat', taskDone: true },
  { taskId: 3, title: 'repeat', taskDone: false },
]

// console.log(Object.keys(todo).map(key => [key, boolean.is((todo as any)[key])]))
