import chance from 'chance'
import { v4 as uuidv4 } from 'uuid'

export const createTask = () => {
  return {
    taskId: uuidv4(),
    title: chance().sentence(),
    taskDone: chance().bool(),
  }
}

export const range = (start: number, stop: number) => {
  const res = []
  for (let i = start; i <= stop; i += 1) {
    res.push(i)
  }
  return res
}

export const createTaskList = (n: number) => {
  return range(0, n).map(i => createTask())
}
