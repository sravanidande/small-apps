import { boolean } from 'io-ts'
import { sortBy } from 'lodash'
import React from 'react'
import { camelCaseToPhrase } from './utils'

type Order = 'asc' | 'desc'

interface CellProps {
  readonly value: any
}

export const Cell: React.FC<CellProps> = ({ value }) => {
  return (
    <td style={{ border: 'solid 1px black' }}>
      {boolean.is(value) ? (
        <input type="checkbox" checked={value} readOnly />
      ) : (
        value
      )}
    </td>
  )
}

interface RowProps<T> {
  readonly row: T
  readonly columns: ReadonlyArray<keyof T>
}

export function Row<T>({ row, columns }: RowProps<T>): JSX.Element {
  return (
    <tr>
      {columns.map((key, i) => (
        <Cell key={i} value={row[key]} />
      ))}
    </tr>
  )
}

interface TableHeaderProps {
  readonly headers: ReadonlyArray<string>
  readonly sortColumn: string
  onHeaderClick(header: string): void
}

function TableHeader({
  headers,
  sortColumn,
  onHeaderClick,
}: TableHeaderProps): JSX.Element {
  return (
    <thead style={{ border: 'solid 1px black' }}>
      <tr style={{ padding: '4px' }}>
        {headers.map(header => {
          const orderStr = header === sortColumn ? '*' : ''
          return (
            <th
              key={header}
              onClick={() => onHeaderClick(header)}
              style={{ border: 'solid 1px black', fontSize: '20px' }}
            >{`${camelCaseToPhrase(header)} ${orderStr}`}</th>
          )
        })}
      </tr>
    </thead>
  )
}

export const toggleOrder = (order: 'asc' | 'desc') => {
  if (order === 'asc') {
    return 'desc'
  } else {
    return 'asc'
  }
}

interface TableProps<T> {
  readonly data: ReadonlyArray<T>
  readonly idKey: keyof T
  readonly columns: ReadonlyArray<keyof T>
  readonly initialSort: keyof T
  readonly perPage: number
}

export function ClientTable<T>({
  data,
  idKey,
  columns,
  initialSort,
  perPage,
}: TableProps<T>): JSX.Element {
  const [order, setOrder] = React.useState<Order>('asc')
  const [sort, setSort] = React.useState<keyof T>(initialSort)
  const [currentPage, setCurrentPage] = React.useState(1)

  const handlePageChange = (page: number) => {
    setCurrentPage(page)
  }
  const handleHeaderClick = (header: string) => {
    if (header === sort) {
      setOrder(toggleOrder(order))
    } else {
      setOrder('asc')
    }
    setSort(header as any)
  }

  const sorted =
    order === 'asc' ? sortBy(data, sort) : sortBy(data, sort).reverse()

  const totalPages = Math.floor((sorted.length + perPage - 1) / perPage)

  const pageValues = sorted.slice(
    currentPage * perPage - perPage,
    currentPage * perPage,
  )

  return (
    <>
      <table style={{ borderCollapse: 'collapse' }}>
        <TableHeader
          headers={columns as any}
          sortColumn={sort as any}
          onHeaderClick={handleHeaderClick}
        />

        <tbody>
          {pageValues.map(row => (
            <Row key={row[idKey] as any} row={row} columns={columns} />
          ))}
        </tbody>
      </table>
      <input
        type="text"
        defaultValue={currentPage}
        onChange={evt => {
          const n = parseInt(evt.target.value, 10)
          if (!isNaN(n)) {
            setCurrentPage(n)
          }
        }}
      />
    </>
  )
}
