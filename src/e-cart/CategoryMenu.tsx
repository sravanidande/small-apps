import React from 'react'
import { categories } from './fakeProducts'

interface CategoryMenuProps {
  readonly categories: ReadonlyArray<string>
  readonly selectedCategory: string
  onCategorySelect(category: string): void
}

export const CategoryMenu: React.FC<CategoryMenuProps> = ({
  categories,
  selectedCategory,
  onCategorySelect,
}) => {
  return (
    <ul>
      {categories.map(category => {
        const style = { backgroundColor: 'skyblue' }
        const categoryStyle = category === selectedCategory ? style : {}
        return (
          <li
            key={category}
            style={categoryStyle}
            onClick={() => onCategorySelect(category)}
          >
            {category}
          </li>
        )
      })}
    </ul>
  )
}
