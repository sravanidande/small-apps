import React from 'react'
import { ProductModalView } from './ProductView'
// import 'bulma/css/bulma.css'

export interface Product {
  readonly id?: string
  readonly name: string
  readonly price: number
  readonly quantity: number
  readonly category: string
}

interface ProductProps {
  readonly product: Product
  onAddProduct(product: Product): void
}

export const Product: React.FC<ProductProps> = ({ product, onAddProduct }) => {
  return (
    <>
      <tr style={{ border: 'solid 1px black', padding: '20px' }}>
        <td>{product.id}</td>
        <td>{product.name}</td>
        <td>{product.price}</td>
        <td>{product.quantity}</td>
        <td>
          <div
            className="button is-primary"
            onClick={() => onAddProduct(product)}
          >
            Add to cart
          </div>
        </td>
        <td>
          <ProductModalView product={product} />
        </td>
      </tr>
    </>
  )
}

interface ProductListProps {
  readonly productList: ReadonlyArray<Product>
  onAddProduct(product: Product): void
  onProductView(product: Product): void
}

export const ProductList: React.FC<ProductListProps> = ({
  productList,
  onAddProduct,
  onProductView,
}) => {
  return (
    <>
      <table style={{ borderCollapse: 'collapse' }}>
        <thead>
          <tr style={{ padding: '4px' }}>
            <th>Id</th>
            <th>Product</th>
            <th>Price</th>
            <th>Quantity</th>
          </tr>
        </thead>
        <tbody>
          {productList.map(product => (
            <Product
              key={product.id}
              product={product}
              onAddProduct={onAddProduct}
            />
          ))}
        </tbody>
      </table>
    </>
  )
}

export interface CartItem {
  readonly product: Product
  readonly count: number
}

interface CartItemProps {
  readonly cartItem: CartItem
  onRemoveItem(cartItem: CartItem): void
  onInc(cartItem: CartItem): void
  onDec(cartItem: CartItem): void
}

export const CartItem: React.FC<CartItemProps> = ({
  cartItem,
  onRemoveItem,
  onInc,
  onDec,
}) => {
  return (
    <>
      <tr style={{ border: 'solid 1px black', padding: '20px' }}>
        <td>{cartItem.product.name}</td>
        <td>{cartItem.product.price}</td>
        <td>
          <p>
            <div className="button is-primary" onClick={() => onInc(cartItem)}>
              +
            </div>
            {cartItem.count}
            <div className="button is-danger" onClick={() => onDec(cartItem)}>
              -
            </div>
          </p>
        </td>
        <td>{cartItem.count * cartItem.product.price}</td>
        <td>
          <button onClick={() => onRemoveItem(cartItem)}>remove</button>
        </td>
      </tr>
    </>
  )
}

interface CartListProps {
  readonly cartList: ReadonlyArray<CartItem>
  onRemoveItem(cartItem: CartItem): void
  onInc(cartItem: CartItem): void
  onDec(cartItem: CartItem): void
}

export const CartList: React.FC<CartListProps> = ({
  cartList,
  onRemoveItem,
  onInc,
  onDec,
}) => {
  return (
    <>
      <h2>Cart items</h2>
      <table style={{ border: 'solid 1px black' }}>
        <thead>
          <tr style={{ border: 'solid 1px black' }}>
            <th>Product</th>
            <th>Price</th>
            <th>Count</th>
            <th>Total Price</th>
          </tr>
        </thead>
        <tbody>
          {cartList.map(item => (
            <CartItem
              key={item.product.id}
              cartItem={item}
              onRemoveItem={onRemoveItem}
              onInc={onInc}
              onDec={onDec}
            />
          ))}
        </tbody>
      </table>
      <h4>
        Total cart price:
        {cartList.reduce((acc, v) => acc + v.product.price * v.count, 0)}
      </h4>

      <hr />
    </>
  )
}
