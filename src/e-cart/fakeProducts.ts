import chance from 'chance'
import { range } from '../makeData'
import { v4 as uuidv4 } from 'uuid'
import { Product } from './Product'

export const categories = ['clothes', 'makeup', 'electronics', 'groceries']

const ch = chance()

export const createProduct = (): Product => {
  const newValue = ch.natural({ min: 0, max: categories.length - 1 })
  return {
    id: uuidv4(),
    name: ch.sentence({ words: 3 }),
    price: ch.natural({ min: 15, max: 120 }),
    quantity: ch.natural({ min: 1, max: 20 }),
    category: categories[newValue],
  }
}

export const fakeProductList = (n: number): ReadonlyArray<Product> =>
  range(0, n).map(i => createProduct())
