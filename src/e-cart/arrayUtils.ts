import { range } from '../makeData'

export function t<T>(
  arr: ReadonlyArray<T>,
  index: number,
  element: T,
): ReadonlyArray<T> {
  const result = []
  for (let i = 0; i < arr.length; i += 1) {
    if (i === index) {
      result.push(element)
      i = i + 1
    }
    result.push(arr[i])
  }
  return result
}

export function t2<T>(
  arr: ReadonlyArray<T>,
  index: number,
  element: T,
): ReadonlyArray<T> {
  const arr1 = arr.slice(0, index)
  const newArray = [...arr1, element]
  const arr2 = arr.slice(index + 1)
  return [...newArray, ...arr2]
}

// export const t3 = (
//   arr: ReadonlyArray<number>,
//   index: number,
//   element: number,
// ): ReadonlyArray<number> => {
// return arr.reduce((acc,v,i)=>{

// },[])
// }

export const insert = (
  arr: ReadonlyArray<number>,
  index: number,
  element: number,
): ReadonlyArray<number> => {
  const result = []
  for (let i = 0; i < arr.length; i += 1) {
    if (i === index) {
      result.push(element)
    }
    result.push(arr[i])
  }
  return result
}

export const insert2 = (
  arr: ReadonlyArray<number>,
  index: number,
  element: number,
): ReadonlyArray<number> => {
  const arr1 = arr.slice(0, index)
  const newArray = [...arr1, element]
  const arr2 = arr.slice(index, arr.length)
  return [...newArray, ...arr2]
}

export function removeAt<T>(
  arr: ReadonlyArray<T>,
  index: number,
): ReadonlyArray<T> {
  return arr.filter((item, i) => i !== index)
}

export const subArray = (
  start: number,
  count: number,
): ReadonlyArray<number> => {
  const result = []
  let item = start
  for (let i = 0; i <= count; i += 1) {
    item = start + i
    result.push(item)
  }
  return result
}

export const subArray2 = (
  start: number,
  count: number,
): ReadonlyArray<number> => {
  const newArray = range(0, count).map((item, i) => {
    item = start + i
    return item
  })
  return newArray
}

export function replaceAt2<T>(
  arr: ReadonlyArray<T>,
  ind: number,
  item: T,
): ReadonlyArray<T> {
  const result = []
  for (let i = 0; i < arr.length; i += 1) {
    if (i === ind) {
      result.push(item)
    } else {
      result.push(arr[i])
    }
  }
  return result
  // const copyArray = [...arr]
  // const newArray = copyArray.splice(ind, 1, item)
  // return copyArray
}

export function filteredArray<T>(
  arr: ReadonlyArray<T>,
  id: string | number,
): ReadonlyArray<T> {
  return arr.filter(item => item.id !== id)
}
