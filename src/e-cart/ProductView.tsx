import React from 'react'
import 'bulma/css/bulma.css'
import { Product } from './Product'

interface ProductModalViewProps {
  readonly product: Partial<Product>
}

export const ProductModalView: React.FC<ProductModalViewProps> = ({
  product,
}) => {
  const [modal, setModal] = React.useState(false)
  console.log(modal)
  const handleModalClick = () => {
    setModal(!modal)
    console.log(modal)
  }
  const className = modal ? 'modal is-active' : 'modal'
  return (
    <>
      <div className="button is-danger" onClick={handleModalClick}>
        View Product
      </div>
      <div className={className}>
        <div className="modal-background"></div>
        <div className="modal-content">
          <div className="box">
            <div className="media">
              <div className="media-left">
                <figure className="image is-64x64">
                  <img
                    src={`https://robohash.org/${product.id}?set=set4`}
                    alt="Placeholder image"
                  />
                </figure>
              </div>
              <div className="media-content">
                <p className="title is-5">Product: {product.name}</p>
                <p className="subtitle is-6">Price: {product.price}</p>
              </div>
            </div>
          </div>
        </div>
        <button
          className="modal-close is-large"
          aria-label="close"
          onClick={handleModalClick}
        ></button>
      </div>
    </>
  )
}
