import React from 'react'
import { BabyInfo } from '../data'

interface BabyListProps {
  readonly babies: ReadonlyArray<BabyInfo>
  onBabyClicked(baby: BabyInfo): void
}

export const BabyList: React.FC<BabyListProps> = ({
  babies,
  onBabyClicked,
}) => (
  <ul>
    {babies.map(baby => (
      <li className={baby.sex} key={baby.id}>
        <button onClick={() => onBabyClicked(baby)}>{baby.name}</button>
      </li>
    ))}
  </ul>
)
