import React from 'react'

export interface BabySearchProps {
  readonly searchText: string
  onBabySearch(name: string): void
}

const style = { margin: 20, padding: 20, border: '2px solid', width: '60%' }

export const BabySearch: React.FC<BabySearchProps> = ({
  searchText,
  onBabySearch,
}) => (
  <header>
    <input
      name="search"
      value={searchText}
      onChange={evt => onBabySearch(evt.target.value)}
      style={style}
      placeholder="Search name here..."
    />
  </header>
)
