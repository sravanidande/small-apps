import React from 'react'

import { babies, BabyInfo } from '../data'

import { BabySearch } from './BabySearch'
import { BabyList } from './BabyList'

export const BabyNamesApp = () => {
  const [search, setSearch] = React.useState('')
  const [shortListed, setShortListed] = React.useState<ReadonlyArray<BabyInfo>>(
    [],
  )

  const filteredBabies = babies.filter(b =>
    b.name.toLowerCase().includes(search.toLocaleLowerCase()),
  )

  const remainingBabies = filteredBabies.filter(b => !shortListed.includes(b))

  const addToShortList = (baby: BabyInfo) => {
    setShortListed([...shortListed, baby])
  }

  const removeFromShortList = (baby: BabyInfo) => {
    setShortListed(shortListed.filter(bi => bi !== baby))
  }

  return (
    <>
      <BabySearch searchText={search} onBabySearch={setSearch} />
      <BabyList babies={shortListed} onBabyClicked={removeFromShortList} />
      <BabyList babies={remainingBabies} onBabyClicked={addToShortList} />
    </>
  )
}
