import React from 'react'
// import './styles.css'
import { CartApp } from './e-cart/CartApp'
// import './styles1.css'
import { WeeklyScheduleView } from './weekly-schedule/WeeklySchedule'
import './weekly-schedule/fakeData'
import { weeklyScheduledData } from './weekly-schedule/fakeData'
import { HomePage } from './e-commerce-app/pages/HomePage'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import { HatsPage } from './e-commerce-app/pages/HatsPage'
import { SignInPageView } from './e-commerce-app/pages/SignInPage'
import { ShopPageView } from './e-commerce-app/pages/ShopPage'
import { shopData } from './e-commerce-app/pages/shopData'
import { Header } from './e-commerce-app/components/Header'
import { auth } from '../src/e-commerce-app/firebase/firebase'
import { User, SignIn } from './e-commerce-app/components/SignIn'
import { MonsterSearchApp } from './monster-search/MonsterSearchApp'

const data = weeklyScheduledData

export const ShopPage: React.FC = () => {
  return <ShopPageView collections={shopData} />
}

// TodoApp
// export const App = () => {
//   const [currentUser, setUser] = React.useState<any>(null)

//   const handleUser = (user: any) => {
//     auth.onAuthStateChanged(user => {
//       setUser(user)
//       console.log(user)
//     })
//   }
//   return (
//     <>
//       <Router>
//         <Header currentUser={currentUser} onUserChange={handleUser} />
//         <Switch>
//           <Route exact path="/" component={HomePage} />
//           <Route exact path="/shop" component={ShopPage} />
//           <Route exact path="/signin" component={SignInPageView} />
//         </Switch>
//       </Router>
//     </>
//   )
// }

// export const App = () => {
//   return <MonsterSearchApp />
// }

export const App = () => {
  return <CartApp />
}

// console.log(weeklyScheduledData)
// console.log(create)
