import React from 'react'
import { CreateNameForm } from './CreateNameForm'
import { NameList } from './NameList'
import { FullName } from './Name'
import { EditForm } from './EditForm'
import { replaceAt2 } from '../../src/e-cart/arrayUtils'

export const CrudApp: React.FC = () => {
  const [nameList, setNameList] = React.useState<ReadonlyArray<FullName>>([])
  const [name, setName] = React.useState<FullName | undefined>(undefined)

  const handleNameChange = (name: FullName) => {
    setNameList([...nameList, name])
  }

  const handleNameDelete = (id: string) => {
    setNameList(nameList.filter(name => name.id !== id))
  }

  const handleNameEdit = (name: FullName) => {
    const idx = nameList.findIndex(user => name.id === user.id)
    const newList = replaceAt2(nameList, idx, name)
    setNameList(newList)
    setName(name)
  }

  return (
    <>
      {name ? (
        <EditForm name={name} onEditName={handleNameEdit} />
      ) : (
        <CreateNameForm onSubmit={handleNameChange} />
      )}
      <NameList
        nameList={nameList}
        onNameDelete={handleNameDelete}
        onNameEdit={handleNameEdit}
      />
    </>
  )
}
