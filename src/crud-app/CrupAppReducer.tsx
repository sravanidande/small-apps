import React from 'react'
import { CreateNameForm } from './CreateNameForm'
import { NameList } from './NameList'
import { FullName } from './Name'
import { EditForm } from './EditForm'
import { filteredArray, replaceAt2 } from '../../src/e-cart/arrayUtils'

interface NameState {
  readonly nameList: readonly FullName[]
  readonly name?: FullName
}

type NameAction =
  | {
      readonly type: 'createName'
      readonly name: FullName
    }
  | {
      readonly type: 'editName'
      readonly name: FullName
    }
  | {
      readonly type: 'deleteName'
      readonly id: FullName['id']
    }

function nameReducer(state: NameState, action: NameAction) {
  switch (action.type) {
    case 'createName': {
      return { nameList: [...state.nameList, action.name], name: action.name }
    }
    case 'deleteName': {
      const filtered = filteredArray(state.nameList, action.id)
      return {
        nameList: filtered,
        name: state.name,
      }
    }
    case 'editName': {
      const idx = state.nameList.findIndex(n => n.id === action.name.id)
      const edited = replaceAt2(state.nameList, idx, name)
      return {
        nameList: edited,
        name: undefined,
      }
    }
    default: {
      throw new Error()
    }
  }
}

export const CrudAppReducer: React.FC = () => {
  // const [nameList, setNameList] = React.useState<ReadonlyArray<FullName>>([])
  // const [name, setName] = React.useState<FullName | undefined>(undefined)

  const [state, dispatch] = React.useReducer(nameReducer, {
    nameList: [],
    name: undefined,
  })

  console.log(state)

  function handleNameCreate(name: FullName) {
    dispatch({ type: 'createName', name })
  }

  const handleNameDelete = (id: string) => {
    dispatch({ type: 'deleteName', id })
  }

  const handleNameEdit = (name: FullName) => {
    dispatch({ type: 'editName', name })
  }

  return (
    <>
      <h1>CrudAppReducer {state.name?.firstName}</h1>
      {state.name ? (
        <EditForm name={state.name} onEditName={handleNameEdit} />
      ) : (
        <CreateNameForm onSubmit={handleNameCreate} />
      )}
      <NameList
        nameList={state.nameList}
        onNameDelete={handleNameDelete}
        onNameEdit={handleNameEdit}
      />
    </>
  )
}
