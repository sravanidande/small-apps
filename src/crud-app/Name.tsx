import React from 'react'

export interface FullName {
  readonly id: string
  readonly firstName: string
  readonly lastName: string
}

interface NameProps {
  readonly name: FullName
  onNameDelete(id: string): void
  onNameEdit(name: FullName): void
}

export const Name: React.FC<NameProps> = ({
  name,
  onNameDelete,
  onNameEdit,
}) => {
  return (
    <li>
      <p>
        {name.id} {name.firstName} {name.lastName}
        <button onClick={() => onNameEdit(name)}>Edit</button>
        <button onClick={() => onNameDelete(name.id)}>delete</button>
      </p>
    </li>
  )
}
