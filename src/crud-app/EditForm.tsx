import React from 'react'
import { FullName } from './Name'
import { v4 as uuidv4 } from 'uuid'

interface EditFormProps {
  readonly name: FullName
  onEditName(name: FullName): void
}

export const EditForm: React.FC<EditFormProps> = ({ name, onEditName }) => {
  const [firstName, setFirstName] = React.useState(name.firstName)
  const [lastName, setLastName] = React.useState(name.lastName)

  return (
    <>
      <form>
        <label htmlFor="name">Name:</label>
        <input
          id="name"
          name="name"
          value={firstName}
          onChange={evt => setFirstName(evt.target.value)}
        />
        <br />
        <label htmlFor="surName">Surname:</label>
        <input
          id="surName"
          name="surName"
          value={lastName}
          onChange={evt => setLastName(evt.target.value)}
        />
        <br />
        <button
          type="submit"
          onClick={evt => {
            evt.preventDefault()
            onEditName({ id: name.id, firstName, lastName })
            setFirstName('')
            setLastName('')
          }}
        >
          Update
        </button>
      </form>
    </>
  )
}
