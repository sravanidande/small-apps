import React from 'react'
import { FullName } from './Name'
import { v4 as uuidv4 } from 'uuid'

interface NameFormViewProps {
  onSubmit(name: FullName): void
}

export const CreateNameForm: React.FC<NameFormViewProps> = ({ onSubmit }) => {
  const [firstName, setFirstName] = React.useState('')
  const [lastName, setLastName] = React.useState('')

  return (
    <>
      <form>
        <label htmlFor="name">Name:</label>
        <input
          id="name"
          name="name"
          value={firstName}
          onChange={evt => setFirstName(evt.target.value)}
        />
        <br />
        <label htmlFor="surName">Surname:</label>
        <input
          id="surName"
          name="surName"
          value={lastName}
          onChange={evt => setLastName(evt.target.value)}
        />
        <br />
        <button
          type="submit"
          onClick={evt => {
            evt.preventDefault()
            onSubmit({ id: uuidv4(), firstName, lastName })
            setFirstName('')
            setLastName('')
          }}
        >
          create
        </button>
      </form>
    </>
  )
}
