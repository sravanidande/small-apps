import React from 'react'
import { Name, FullName } from './Name'

interface NameListProps {
  readonly nameList: readonly FullName[]
  onNameDelete(id: string): void
  onNameEdit(name: FullName): void
}

export const NameList: React.FC<NameListProps> = ({
  nameList,
  onNameDelete,
  onNameEdit,
}) => {
  return nameList && nameList.length === 0 ? null : (
    <>
      <h1>Users List</h1>
      <ul>
        {nameList.map(name => (
          <Name
            key={name.firstName}
            name={name}
            onNameDelete={onNameDelete}
            onNameEdit={onNameEdit}
          />
        ))}
      </ul>
    </>
  )
}
