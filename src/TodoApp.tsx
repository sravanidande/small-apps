import React from 'react'
import { ClientTable } from './ClientTable'
import { createTaskList } from './makeData'

export const TodoApp: React.FC = () => {
  return (
    <>
      <ClientTable
        data={createTaskList(30)}
        idKey="taskId"
        columns={['taskId', 'title', 'taskDone']}
        initialSort="taskId"
        perPage={7}
      />
    </>
  )
}
